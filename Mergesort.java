
public class Mergesort extends Thread{
    private int[] numeros;
    private int[] aux;

    private int numero;
    
    public Mergesort(int arr[]){
    	this.numeros=arr;

    }

    public void sort(int[] datos) {
        this.numeros = datos;
        numero = datos.length;
        this.aux = new int[numero];
        mergeSort(0, numero - 1);
    }

    private void mergeSort(int menor, int mayor) {

        if (menor < mayor) {
            int mitad = menor+(mayor-menor)/2;
            mergeSort(menor, mitad);
            mergeSort(mitad+1,mayor);
            merge(menor, mitad, mayor);
        }
    }

    private void merge(int menor, int mitad, int mayor) {

        for (int i = menor; i <= mayor; i++) {
            aux[i] = numeros[i];
        }

        int i = menor;
        int j = mitad + 1;
        int k = menor;

        while (i <= mitad && j <= mayor) {
            if (aux[i] <= aux[j]) {
                numeros[k] = aux[i];
                i++;
            } else {
                numeros[k] = aux[j];
                j++;
            }
            k++;
        }

        while (i <= mitad) {
            numeros[k] = aux[i];
            k++;
            i++;
        }
        toString();
    }
    public String toString(){
    	
    	for(int i=0;i<numeros.length;i++){

    		System.out.print( numeros[i]+" ");
    	}
    	System.out.println();
    	
    	return null;
    } 
    public void run (){
    	
    	sort(numeros);
    }
}
