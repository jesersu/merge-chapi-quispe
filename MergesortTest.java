import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Random;

import org.junit.Before;

public class MergesortTest {

    private int[] numeros;
    private int size = 7;
    private int max = 20;

    @Before
    public void setUp() throws Exception {
        numeros = new int[size];
        Random generator = new Random();
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = generator.nextInt(max);
        }
    }

    @Test
    public void testMergeSort() {
        long startTime = System.currentTimeMillis();

        Mergesort sorter = new Mergesort();
        sorter.sort(numeros);

        long stopTime = System.currentTimeMillis();
        long tiempoTotal = stopTime - startTime;
        System.out.println("Mergesort " + tiempoTotal);

        for (int i = 0; i < numeros.length - 1; i++) {
            if (numeros[i] > numeros[i + 1]) {
                fail("No deberia de suceder");
            }
        }
        assertTrue(true);

    }
    @Test
    public void testSort() {
        for (int i = 0; i < 200; i++) {
            numeros = new int[size];
            Random generador = new Random();
            for (int a = 0; a < numeros.length; a++) {
                numeros[a] = generador.nextInt(max);
            }
            Mergesort sorter = new Mergesort();
            sorter.sort(numeros);
            for (int j = 0; j < numeros.length - 1; j++) {
                if (numeros[j] > numeros[j + 1]) {
                    fail("No deberia suceder");
                }
            }
            assertTrue(true);
        }
    }
    @Test
    public void testJavaSort() {
        long startTime = System.currentTimeMillis();
        Arrays.sort(numeros);//metodo de sort de java
        long stopTime = System.currentTimeMillis();
        long tiempoTotal = stopTime - startTime;
        System.out.println("Metodo Sort de java" + tiempoTotal);

        for (int i = 0; i < numeros.length - 1; i++) {
            if (numeros[i] > numeros[i + 1]) {
                fail("No deberia suceder");
            }
        }
        assertTrue(true);
    }

}
