import java.util.Random;

class Transmitter<T> extends Thread{
  private Port p;
  public Transmitter(Port p){ 
	  this.p = p; 
  }
  public void run(){
	Random rand = new Random();
    Integer[] numeros = new Integer[10];
    for (int i=0; i<numeros.length; i++) {
       numeros[i] = rand.nextInt(10);
       System.out.println("Enviando el mensaje");
       T m = (T)numeros[i];
       p.send(m);
       System.out.println("Mensaje enviado, sigo con mi vida!");
    }   
    
  }
}
