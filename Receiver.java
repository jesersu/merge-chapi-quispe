import java.util.*;

class Receiver <T> extends Thread {
  private Port p;
  private List datos= new ArrayList<T>();

  public Receiver(Port p){ this.p = p; }
  public List<T> getDatos() {
	  return datos;
  }
  public void merge(T m){
	  int posicion=0;
	  Integer num = (Integer) m;
	  for(int i=0;i<datos.size();i++){
		  Integer elem= (Integer) datos.get(i);
		  if(num>elem)
			  posicion++;
	  }
	  datos.add(posicion,m);
	}
  public void run(){
	 while(true){
		System.out.println("Esperando recibir un numero");
		T m = (T)p.receive();
		if(datos.size()==0){
			datos.add(m);
		}
		else{
			merge(m);
		}
		System.out.println("Numero recibido" + datos);
		p.reply(m);
	}	
	 
  }
  
}


